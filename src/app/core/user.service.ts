import { Injectable, Optional } from '@angular/core';

export class UserServiceConfig {
  userName = 'Guest';
}

@Injectable()
export class UserService {
  private _userName = 'Sherlock Holmes';

  constructor(@Optional() config: UserServiceConfig) {
    if (config) {
      this._userName = config.userName;
    }
  }

  get userName() {
    return this._userName;
  }
}
