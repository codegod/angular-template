import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

/* Core Modules */
import { CoreModule } from '@app/core';

/* Routing Module */
import { AppRouting } from './app.routing';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    CoreModule.forRoot({ userName: 'Miss Marple' }),
    AppRouting,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
