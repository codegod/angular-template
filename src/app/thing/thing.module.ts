import { NgModule } from '@angular/core';

import { SharedModule } from '@app/shared';

import { ThingComponent, ThingDetailComponent, ThingListComponent } from '.';
import { ThingRoutingModule } from './thing.routing';

@NgModule({
  imports: [SharedModule, ThingRoutingModule],
  declarations: [ThingComponent, ThingDetailComponent, ThingListComponent],
})
export class ThingModule {}
