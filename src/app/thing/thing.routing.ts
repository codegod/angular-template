import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ThingComponent, ThingListComponent, ThingDetailComponent } from '.';

const routes: Routes = [
  {
    path: '',
    component: ThingComponent,
    children: [
      { path: '', component: ThingListComponent },
      { path: ':id', component: ThingDetailComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ThingRoutingModule {}
