import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Thing, ThingService } from './thing.service';

@Component({
  template: `
    <h3 highlight>Thing Detail</h3>
    <div *ngIf="thing">
      <div>Id: {{thing.id}}</div><br>
      <label>Name:
        <input [(ngModel)]="thing.name">
      </label>
    </div>
    <br>
    <a routerLink="../">Thing List</a>
  `,
})
export class ThingDetailComponent implements OnInit {
  thing: Thing;

  constructor(
    private route: ActivatedRoute,
    private thingService: ThingService,
  ) {}

  ngOnInit() {
    const id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.thingService
      .getThing(id)
      .subscribe((thing: Thing) => (this.thing = thing));
  }
}
