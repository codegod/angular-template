import { Injectable, OnDestroy } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { delay } from 'rxjs/operators';

export class Thing {
  constructor(public id: number, public name: string) {}
}

const THINGS: Thing[] = [
  new Thing(11, 'Mr. Nice'),
  new Thing(12, 'Narco'),
  new Thing(13, 'Bombasto'),
  new Thing(14, 'Celeritas'),
  new Thing(15, 'Magneta'),
  new Thing(16, 'RubberMan'),
];

const FETCH_LATENCY = 500;

/** Simulate a data service that retrieves heroes from a server */
@Injectable()
export class ThingService implements OnDestroy {
  constructor() {
    console.log('HeroService instance created.');
  }
  ngOnDestroy() {
    console.log('HeroService instance destroyed.');
  }

  getThings(): Observable<Thing[]> {
    return of(THINGS).pipe(delay(FETCH_LATENCY));
  }

  getThing(id: number | string): Observable<Thing> {
    return of(THINGS.find((thing: Thing) => thing.id === +id)).pipe(
      delay(FETCH_LATENCY),
    );
  }
}
