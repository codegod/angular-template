import { Component } from '@angular/core';

import { ThingService } from './thing.service';
import { UserService } from '../core/user.service';

@Component({
  template: `
    <h2>Things of {{userName}}</h2>
    <router-outlet></router-outlet>
  `,
  providers: [ThingService],
})
export class ThingComponent {
  userName = '';
  constructor(userService: UserService) {
    this.userName = userService.userName;
  }
}
