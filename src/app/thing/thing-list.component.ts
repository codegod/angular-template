import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Thing, ThingService } from './thing.service';

@Component({
  template: `
    <h3 highlight>Thing List</h3>
    <div *ngFor='let thing of things | async'>
      <a routerLink="{{thing.id}}">{{thing.id}} - {{thing.name}}</a>
    </div>
  `,
})
export class ThingListComponent {
  things: Observable<Thing[]>;
  constructor(private thingsService: ThingService) {
    this.things = this.thingsService.getThings();
  }
}
